.. title: openATTIC 2.0.5 beta has been released
.. slug: openattic-205-beta-has-been-released
.. date: 2015-12-16 12:01:33 UTC+01:00
.. tags: announcement, collaboration, contributing, news, beta, opensource, release
.. category:
.. link:
.. description: Announcing the openATTIC 2.0.5 beta release
.. type: text
.. author: Laura Paduano

Right on time before the holiday season, we're happy to announce a new beta
release of openATTIC.

.. thumbnail:: ../../images/openattic-logo-wallpaint.jpg

  openATTIC logo wall painting!

For this release, we fixed several bugs and usability issues in the GUI and
extended the RESTful API test cases as well as the WebUI tests. Furthermore,
the openATTIC backend code and RESTful API have been extended with the
functionality to grow unformatted DRBD connections.

.. TEASER_END

The documentation also received several updates. For example, we improved the
instructions that describes how to set up and configure a development
environment. The Ceph installation chapter was also slightly updated and
promoted.

On the packaging side, we removed some obsolete package dependencies and made
some fixes regarding some services like Samba, rpcbind and added the
openattic-module-lio RPM package to the openattic meta RPM package
dependencies.

The openATTIC backend will now load some additional iSCSI-related kernel
modules, which should fix some "fabric iscsi not loaded" errors that were
observed during testing.

Starting with openATTIC version 2.0.5, we have switched to a different process
for submitting and accepting code contributions. We now use the
"signed-off-by" procedure that was originally developed by the Linux kernel
community. For more information, please see `openATTIC Contributing Guidelines
<http://docs.openattic.org/2.0/developer_docs/contributing_guidelines.html>`_
and our related :doc:`blog post <contributing-patches-and-reporting-bugs>`.

We would like to thank everyone who contributed to this release!

Please note that 2.0.5 is still a **beta version**. As usual, handle with care
and make sure to create backups of your data!

Your feedback, ideas and bug reports are very welcome. If you would like to
get in touch with us, consider joining our `openATTIC Users
<https://groups.google.com/forum/#%21forum/openattic-users>`_ Google Group,
visit our `#openattic <irc://irc.freenode.org/#openattic>`_ channel on
`irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_ or leave
comments below this blog post.

See the list below for a more detailed change log and further references. The
OP codes in brackets refer to individual Jira issues that provide additional
details on each item. You can review these on our `public Jira instance
<http://tracker.openattic.org/>`_.

* Web UI: Fixed full screen mode. (:issue:`OP-700`)
* Web UI: Snapshots are sortable by 'create date' now (:issue:`OP-814`)
* Web UI: Fixed checking for a name that has been already taken while editing
  users or hosts. (:issue:`OP-796`)
* Web UI: Checking for an unique name for a new user now (:issue:`OP-588`)
* Web UI: Restrict unauthorized access to the UI (:issue:`OP-748`)
* Web UI: Deactivated menu entry "iSCSI/FC" for physical disks listed in
  volumes (:issue:`OP-542`)
* Web UI: Changed Dashboard, Disks, Pools, Volumes, System and API-Recorder
  icons to clearer ones
* WebUI/QA: Fixed user E2E test which did not work properly in chromium
  anymore (see :issue:`OP-821`)
* WebUI/QA: Added E2E test for creating a new user, app should warn about
  existing user names (:issue:`OP-588`)
* Web UI/QA: Added E2E test for bugfix :issue:`OP-748` (Clicking the openATTIC icon on
  the login page bypasses the login prompt)
* Web UI/QA: Added E2E test for :issue:`OP-542`, relates to :issue:`OP-597`; Test checks that
  the storage tab isn't visible anymore for pysical disks (:issue:`OP-789`)
* Web UI/QA: Test to verify unique names in user and host editing mode
  (:issue:`OP-797`)
* Backend: Added loading of kernel modules "target_core_iblock" and
  "target_core_pscsi" to the LIO backend, to fix a "fabric iscsi not loaded"
  error observed on CentOS 7 (:issue:`OP-722`)
* Backend: Added RESTful API functionality to grow unformatted DRBD
  connections and related Gatling API tests (:issue:`OP-667`, :issue:`OP-707`)
* Installation: Removed dependency on the djextdirect Python module from the
  RPM and DEB packages, removed obsolete file backend/rpcd/extdirect.py
  (:issue:`OP-784`)
* Installation: Added call to start and enable the rpcbind service before
  starting the NFS server in the openattic-module-nfs RPM package (:issue:`OP-786`)
* Installation: Removed obsolete dependency on the Oxygen icon set from the
  RPM package dependencies (:issue:`OP-787`)
* Installation: Make sure to start and enable Samba when installing the
  openattic-module-samba RPM on RHEL7 and derivatives (:issue:`OP-788`)
* Installation: Updated the RPM package installation to no longer install all
  files as owned by the openattic user (:issue:`OP-819`)
* Packaging: Added openattic-module-lio to the openattic meta RPM package
  dependencies, moved dependency on python-rtslib from the openattic-base
  package to the openattic-module-lio RPM, as it's the only one that requires
  it (and to be in line with dependencies defined by the DEB packages)
