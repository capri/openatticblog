.. title: Speaking at the Vault 2017
.. slug: speaking-at-the-vault-2017
.. date: 2017-03-03 10:48:27 UTC+01:00
.. tags: ceph, community, conference, development, openattic, storage, vault
.. category: 
.. link: 
.. description: Announcing the openATTIC talk at Vault Conference 2017 in Cambridge, MA
.. type: text
.. author: Patrick Nawracay

For your information, I'll be talking about Ceph and storage management with
openATTIC on Wednesday, March 22th at 12:00 EST (19:00 UTC) at the `Vault 2017
<http://events.linuxfoundation.org/events/vault>`_.

Vault brings together the leading developers and users in file systems and
storage in the Linux kernel and related projects to forge a path to continued
innovation and education.

This presentation will give an overview and update about what has happend in
the last year of the development of openATTIC, what has changed since the
project has been acquired by SUSE and where we are heading to. A general
introduction to openATTIC as well as an outline about new features and
impovements to openATTIC with regard to the traditional storage using NFS,
CIFS, Samba, iSCSI or FC and distributed storage management and monitoring
using Ceph.

Come to my talk and register now to save 20% off your pass with code VLTSP20 -
`Register <http://bit.ly/2kymdDs>`_

I look forward to see you there!
