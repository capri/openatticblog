.. title: Using Docker for openATTIC Development
.. slug: using-docker-for-openattic-development
.. date: 2017-02-15 16:16:56 UTC+01:00
.. tags: contributing, community, development, docker
.. category:
.. link:
.. description: Introducing the openattic-docker project
.. type: text
.. author: Lenz Grimmer

.. image:: /images/docker-logo-small-h.png

In addition to using `Vagrant
<http://docs.openattic.org/2.0/developer_docs/vagrant.html#setting-up-a-development-system-with-vagrant>`_
for working on the openATTIC code base, it is now also possible to set up a
development environment based on `Docker <https://www.docker.com/>`_ containers.

Thanks to our colleague `Ricardo Dias <https://rjd15372.github.io/>`_ from the
SUSE Enterprise Storage Team, you can spin up docker images based on either
openSUSE Leap 42.2 or Ubuntu 16.04 LTS "Xenial" that include everything to get
openATTIC up and running. The Docker container actually maps the Mercurial code
repository from the host development system into the runtime environment.

This way, you can use your preferred editor of choice and any local changes can
be tested in the container immediately.

You can find the required Dockerfiles and support scripts in the
`openattic-docker <https://github.com/openattic/openattic-docker>`_ git
repository on github. The ``README.md`` file included in the repository explains
how to get started.

We hope, that this gives you more choice for setting up a developent environment
for openATTIC quickly, without having to make any changes to your host operating
system.

As usual, we welcome your feedback and contributions!