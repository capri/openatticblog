.. title: openATTIC 2.0.17 beta has been released
.. slug: openattic-2017-beta
.. date: 2016-12-23 10:05:50 UTC+01:00
.. tags: announcement, collaboration, contributing, news, beta, opensource, release
.. category: 
.. link: 
.. description: Announcing the 2.0.17 beta release
.. type: text
.. author: Volker Theile

Shortly before the holidays, we're happy to announce the availability of
openATTIC version 2.0.17!

Due to the onboarding of the openATTIC team to SUSE, this release took a bit
longer than the usual cycle. But we hope it was worth waiting for!

As usual, we included a good mix of bug fixes, improvements and some new
functionality. Some highlights in this version include:

* Lots of improvements for installing and running openATTIC on Ubuntu Linux
  16.04 aka "Xenial Xerus". openATTIC on Xenial now passes all tests and the
  installation should be fairly straightforward. We're still interested in your
  feedback, though - please let us know if you still find any issues installing
  and running openATTIC on this platform. An installation how-to is available in
  the last :doc:`Release announcement <openattic-2016-beta>`, an update of the
  installation documentation will also be provided.
* In the Ceph backend, we moved calls to librados into separate processes, to
  prevent potential blocking of the Django application and Web UI in case that
  RADOS calls get stuck.
* We've replaced the previous systemd DBUS calls with calls to ``systemctl`` and
  now use ``systemd`` for starting/stopping/reloading services on all platforms
  where ``systemd`` is available (previously, openATTIC was still using the
  "old" SysV init tools, e.g. ``service``). We also switched to using
  ``reload-or-restart`` for reloading services by default.
* The Web UI received a number of refinements and improvements, e.g. some
  API-Recorder fixes and usability enhancements. Now, it's also possible to
  obtain a user's authentication token via the web interface, which helps to
  avoid using passwords in external scripts or applications that want to access
  the openATTIC REST API.
* During the initial installation on RPM-based systems, the openattic PostgreSQL
  database and user account are now created using a random password via
  ``oaconfig install``.
* Improvements for setting up development environments using Vagrant.
* We also added a new chapter to the Documentation that describes `how to set
  up a multi-node configuration
  <http://docs.openattic.org/2.0/install_guides/multinode_setup.html>`_.

.. TEASER_END

To download and install openATTIC, please follow the `installation instructions
<http://docs.openattic.org/2.0/install_guides/index.html>`_ in the
documentation.

Please note that 2.0.17 is still a **beta version**. As usual, handle with care
and make sure to create backups of your data!

We would like to thank Ricardo Marques and everyone else who contributed to this
release.

Your feedback, ideas and bug reports are very welcome. If you would like to get
in touch with us, consider joining our `openATTIC Users
<https://groups.google.com/forum/#%21forum/openattic-users>`_ Google Group,
visit our `#openattic <irc://irc.freenode.org/#openattic>`_ channel on
`irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_ or leave
comments below this blog post.

See the list below for a more detailed change log and further references. The OP
codes in brackets refer to individual Jira issues that provide additional
details on each item. You can review these on our `public Jira instance
<http://tracker.openattic.org/>`_.

Changelog for 2.0.17:

* Backend: RPC API dependencies removed in ``oaconfig`` (:issue:`OP-1255`)
* Backend: Infinite hanging librados calls are avoided by separated processes
  with timeouts (:issue:`OP-1340`)
* Backend: Fixed creation of NFS and Samba shares on Ubuntu Xenial
  (:issue:`OP-1697`)
* Backend: Fixed XML-RPCD on Ubuntu Xenial (:issue:`OP-1698`)
* Backend: Fixed creation of volume snapshots on Xenial (:issue:`OP-1699`)
* Backend: Fixed btrfs not being available when adding a new volume
  (:issue:`OP-1726`)
* Backend: Fixed pagination on Xenial (:issue:`OP-1727`)
* Backend: Fix Nagios scripts to get the volume usage statistics working again
  (:issue:`OP-1403`)
* Backend: Fixed compatibility issues in our Ceph module on Xenial
  (:issue:`OP-1763`)
* Backend: Fixed calls to ``systemd`` (not oA systemd) (:issue:`OP-1769`)
* Backend: Fixed truncated dropdown inputs on Xenial (:issue:`OP-1788`)
* Backend: Use ``reload-or-restart`` instead of plain ``reload`` for restarting
  services on systemd-based operating systems (:issue:`OP-984`).
* Backend: Change taskqueue description and result fields to ``TextField``
  (:issue:`OP-1755`)
* Installation: Fixed Apache redirect from ``/`` to ``/openattic`` on Ubuntu
  Xenial (:issue:`OP-1672`)
* Installation: Replaced hard-coded paths in systemd unit files with ``OADIR``
  variable. Thanks to Ricardo Marques. (PR#522)
* WebUI: Display and generate a user's API auth token. (:issue:`OP-815`)
* WebUI: Render the Disks/Pools/Volumes indication of size in the frontend
  (:issue:`OP-1115`)
* WebUI: Display alternative graph image if loading fails (:issue:`OP-1403`)
* WebUI: Make the Disks/Pools/Volumes datatable sortable by additional criteria
  (:issue:`OP-1427`)
* WebUI: Add a button to the API Recorder dialog to copy the Python script into
  clipboard. (:issue:`OP-1706`)
* WebUI: Fixes iSCSI/FC wizard issue (:issue:`OP-1718`)
* WebUI: Modify the API-Recorder Start/Stop buttons (:issue:`OP-1720`)
* WebUI: Display tooltips again (:issue:`OP-1749`)
* WebUI: Remove the OSD selection (:issue:`OP-1615`)
* WebUI: Prevent the disabling of all columns in oA-datatables
  (:issue:`OP-1158`)
* WebUI: Validate the right WWN formats in the selected host share method
  (:issue:`OP-1786`)
* WebUI: Fixed browsing the Ceph pool statistics (:issue:`OP-1743`)
* WebUI/QA: E2E adjustments related to OP-1786 (:issue:`OP-1787`)
* WebUI/QA: Try to disable all columns and expect not to succeed
  (:issue:`OP-1718`)
* WebUI/QA: refactored cmdlog e2e tests (:issue:`OP-1797`)
* Installation: Fix Permission denied: ``/usr/bin/pip`` for Vagrant development
  environment using SUSE Leap (:issue:`OP-1715`)
* Installation: New database instances are created using a random password. This
  change applies to RPM packages only. (:issue:`OP-1438`)
* Installation: Improved error message for problems with ``database.ini``
  (:issue:`OP-1739`)
* Installation: Fix a bug with recreation of the database (:issue:`OP-1757`)
* Installation: Properly restart ``npcd`` in ``oaconfig install`` on
  distributions using systemd (:issue:`OP-1688`)
* Documentation: Applied some fixes contributed by Ricardo Marques (PR#523)
* Development: Fix storage controller issue with the Vagrant Debian box.
* Development: Fix patch file which is customizing settings in a Vagrant VM.
* Development: Fix vagrant-cachier issues. (:issue:`OP-1466`)
* Documentation: Added chapter about multinode setup (:issue:`OP-1045`)