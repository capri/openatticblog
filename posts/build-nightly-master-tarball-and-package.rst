.. title: Build nightly master tarball and package
.. slug: build-nightly-master-tarball-and-package
.. date: 2017-10-20 15:38:10 UTC+02:00
.. tags: build nightly packages tarball jenkins 
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Kai Wagner

I'm very glad to announce that we have our automatic nightly master tarball and package build functional again.

I've you're interested in the latest code we're currently working on, just checkout our nightly master build. The package and tarball will be updated every hour if there has been a commit since the last build.

Link to: `Source <http://download.openattic.org/sources/nightly-master-3.x/>`_
Link to: `Package <http://download.openattic.org/rpm/openattic-nightly-master-openSUSE_Leap_42.3-x86_64/>`_
Link to: `Installation Guide <http://docs.openattic.org/en/latest/install_guides/oA_installation.html#installation-on-opensuse-leap>`_
