.. title: openATTIC 2.0.10 beta has been released
.. slug: openattic-2.0.10-beta-has-been-released
.. date: 2016-04-21 11:29:29 UTC+02:00
.. tags: announcement, collaboration, contributing, news, beta, opensource, release
.. category: 
.. link: 
.. description: Announcing the openATTIC 2.0.10 beta release
.. type: text
.. author: Patrick Nawracay

Yesterday, we released version 2.0.10 of openATTIC. The highlights of this release are our re-designed GUI and the ability to retrieve a list of configured Ceph pools with their configuration details.

As mentioned earlier in our last `Sneak preview of the openATTIC GUI redesign <https://blog.openattic.org/posts/sneak-preview-of-the-openattic-gui-redesign/>`_ post, the new design was necessary after we have decided to drop the "SmartAdmin" template. You may take a look at the re-desidned GUI on our demo system.

Furthermore, version 2.0.10 comes with a new Ceph related GUI integration. You may have noticed the new ``Pools`` menu item which can be found under the ``Ceph`` menu. If you have ``ceph`` and ``openattic-module-ceph`` installed and appropriately configured on your openATTIC host, you will be able to see the list of available pools of your Ceph cluster with their corresponding details. The appearance of those details is going to be refined in our next releases.

Also, we fixed a bunch of bugs, extended and updated the documentation and took care of quality assurance as well.

.. TEASER_END

Please note that 2.0.10 is still a **beta version**. As usual, handle with care and make sure to create backups of your data!

We would like to thank everyone who contributed to this release! Special thanks to the folks from SUSE for their feedback and support.

Your feedback, ideas and bug reports are very welcome. If you would like to get in touch with us, consider joining our `openATTIC Users <https://groups.google.com/forum/#%21forum/openattic-users>`_ Google Group, visit our `#openattic <irc://irc.freenode.org/#openattic>`_ channel on
`irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_ or leave comments below this blog post.

See the list below for a more detailed change log and further references. The OP codes in brackets refer to individual Jira issues that provide additional details on each item. You can review these on our `public Jira instance <http://tracker.openattic.org/>`_.

Changelog 2.0.10:

* WebUI: Show an error message if JavaScript is disabled (:issue:`OP-580`)
* WebUI: Remove Smartadmin and added a new openATTIC theme (:issue:`OP-585`)
* WebUI: replaced all SmallBox notifications by toasty notifications (:issue:`OP-720`)
* WebUI: Disable resize for btrfs volumes (:issue:`OP-911`)
* WebUI: New pagination template/layout (:issue:`OP-1003`)
* WebUI: Accordion header is now clickable (:issue:`OP-1064`)
* WebUI: Add d3, nvd3 and angular-nvd3 (:issue:`OP-1074`)
* WebUI: Ceph pool data table (:issue:`OP-867`)
* WebUI/QA: Refactor and add tests to volume_resize.e2e.js (:issue:`OP-912`)
* WebUI/QA: Added afterAll function to all tests (:issue:`OP-980`)
* WebUI/QA: Added e2e tests to test the pagination (:issue:`OP-1010`)
* WebUI/QA: Ceph menu related e2e tests (:issue:`OP-945`)
* WebUI/QA: Added very basic test case for ceph pool overview (:issue:`OP-1102`)
* Backend: Added basic infrastructure to directly gather information from Ceph clusters (:issue:`OP-1014`)
* Backend: List all existing Pools and their characteristics (:issue:`OP-751`)
* Backend: Fixed a bug with the CRUSH map which caused the server to loop endlessly (:issue:`OP-865`)
* Backend: Let the ceph module fail graciously (:issue:`OP-1022`)
* Backend: Implement the pagination for Ceph pools (:issue:`OP-1026`)
* Backend: Added additional information to be retrieved about Ceph pools using the RESTful API (:issue:`OP-1053`)
* Backend: The name of a netdevice allows a length of up to 15 characters now (:issue:`OP-960`) (thanks to Willi Fehler for reporting this issue)
* Backend: Fixed bug - add local volumes to local fstab only (:issue:`OP-1058`)
* Backend: Fixed bug - removing an Ext3/Ext4/XFS filesystem volume that exports an nfs share didn't work properly (:issue:`OP-736`)
* Backend/Gatling: Cleaned up Gatling (:issue:`OP-1025`)
* Installation: "oaconfig install" now skips all LVM volume groups tagged with "sys", so they won't be added to the oA database (:issue:`OP-564`). Fixed oaconfig  add-vg <volume group> so that it aborts if the VG does not exist or is tagged as "sys". We have also updated the installation documentation and replaced "@sys" with "sys" (LVM strips the @-sign from the tag)
* Installation: Fixed deprecation warning when importing rtslib on systems that  ship rtslib-fb (e.g. EL7) (:issue:`OP-953`)
* Installation: marked openattic.repo in the RPM subpackage openattic-release  as a configuration file, to protect it from getting mangled by RPM updates (:issue:`OP-987`)
* Installation: Updated RPM and DEB packaging to include the new "nodb" Python modules, added dependency on the "python-rados" package (:issue:`OP-1021`)
* Documentation: Added Gatling document to openATTIC documentation (:issue:`OP-894`)
