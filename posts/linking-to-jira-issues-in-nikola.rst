.. title: Linking to Jira issues in Nikola using the issue_role plugin
.. slug: linking-to-jira-issues-in-nikola
.. date: 2016-03-03 17:17:15 UTC+01:00
.. tags: collaboration, contributing, nikola, opensource, python
.. category: 
.. link: 
.. description: Announcing the issue_role Nikola plugin
.. type: text
.. author: Lenz Grimmer

Our release announcements (like the :doc:`one for openATTIC 2.0.8
<openattic-208-beta-has-been-released>`) usually contain a lot of references
to issues on `our issue tracker <http://tracker.openattic.org>`_ in the form
of issue ids like `OP-XXX`.

However, `Nikola <https://getnikola.com>`_, the static site generator that
powers this blog, did not provide an easy method to convert these issue IDs
into URLs pointing to our issue tracker automatically. Converting each issue
ID into an URL manually using the existing reStructuredText link syntax felt
too tedious.

But fortunately Nikola provides a plugin system, that allows you to `extend
its functionality <https://getnikola.com/extending.html>`_ quite easily!

After some discussion with the Nikola developers on the #nikola IRC channel on
Freenode, we came to the conclusion that the best approach would be to extend
the reStructuredText markup language with an additional `explicit role
<http://docutils.sourceforge.net/docs/ref/rst/roles.html>`_.

And that's what we did - our `pull request
<https://github.com/getnikola/plugins/pull/135>`_ was accepted and the new
plugin `issue_role <https://plugins.getnikola.com/#issue_role>`_ is now
available from the Nikola plugin repository.

The installation and configuration is pretty simple - simply run ``nikola
plugin --install issue_role`` and configure ``conf.py`` as described, by
setting ``ISSUE_URL`` in the ``GLOBAL_CONTEXT``. In our case, we added the
following to the configuration file::

  GLOBAL_CONTEXT['ISSUE_URL'] = "http://tracker.openattic.org/browse/{issue}"

In our blog posts, we now enter issues as follows, and Nikola takes care of
converting them into URLs::

  Created plugin that automatically converts Jira Task IDs to URLs
  (:issue:`OP-741`)

Running `nikola build` converts this into the following HTML:

  Created plugin that automatically converts Jira Task IDs to URLs
  (:issue:`OP-741`)

We hope you find this useful!
