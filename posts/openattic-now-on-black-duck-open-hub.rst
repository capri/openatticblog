.. title: openATTIC now on Black Duck Open Hub
.. slug: openattic-now-on-black-duck-open-hub
.. date: 2015-09-14 18:47:53 UTC+02:00
.. tags: news, opensource
.. category: 
.. link: 
.. description: openATTIC has been added to Black Duck Open Hub
.. type: text
.. author: Lenz Grimmer

We've recently added openATTIC to the `Black Duck Open Hub
<http://openhub.net/p/openattic/>`_. If you haven't heard about Open Hub
(formerly known as "Ohloh.net"), here's how they describe themselves:

  The Black Duck Open Hub (formerly Ohloh.net) is an online community and
  public directory of free and open source software (FOSS), offering analytics
  and search services for discovering, evaluating, tracking, and comparing
  open source code and projects. Open Hub Code Search is free code search
  engine indexing over 21,000,000,000 lines of open source code from projects
  on the Black Duck Open Hub.

.. TEASER_END

The source code analysis performed by Open Hub revealed a few interesting
insights:

  In a Nutshell, openattic...

  ... has had 6,686 commits made by 14 contributors representing 28,094 lines
  of code

  ... is mostly written in Python with an average number of source code
  comments

  ... has a codebase with a long source history maintained by a large
  development team with increasing Y-O-Y commits

  ... took an estimated 7 years of effort (COCOMO model) starting with its
  first commit in January, 2011 ending with its most recent commit 4 days ago

The `languages summary
<https://www.openhub.net/p/openattic/analyses/latest/languages_summary>`_ also
shows some interesting turning points caused by refactoring or changes in the
underlying technologies used, e.g. in September 2013, where the amount of
JavaScript and CSS code was drastically reduced, while the amount of Python
code increased. According to the developers, this might have been caused by
the switch from one version of Ext JS to a newer version. Also, merge of the
new Web UI code based on `AngularJS <https://angularjs.org/>`_ has left a
notable mark in the June/July 2015 time frame.

It will be interesting to follow the future development of the project via
this tool!
