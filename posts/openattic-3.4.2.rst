.. title: openATTIC 3.4.2 has been released
.. slug: openattic-342-has-been-released
.. date: 2017-08-04 12:35:53 UTC
.. tags: announcement, ceph, collaboration, contributing, news, beta, opensource, release
.. category:
.. link:
.. description: Announcing the openATTIC 3.4.2 release
.. type: text
.. author: Volker Theile

We are very happy to announce the release of openATTIC version 3.4.2.

In this release, we've continued with the integration of Ceph Luminous features. It is now possible to configure the Ceph keyring via the 'System | Settings' menu.
This release also implements the WebUI part of the previously introduced backend feature to create erasure coded overwrite enabled pools.
openATTIC now also supports to enable compression on pools stored on OSDs with the "Bluestore" storage backend.
The WebUI will notify you about RBD features that are not supported when you create a new iSCSI target.
Developers will benefit from the ability to overwrite global settings for the backend and frontend via local settings files.

Beside the new features mentioned above, this release also includes various bug fixes and minor improvements. It is possible to delete non-empty RGW buckets now,
but please take care, this will also remove buckets that are still in use, e.g. by NFS Ganesha. When a new RBD is created, the old format is not used anymore.

.. TEASER_END

We would like to thank everyone who contributed to this release.

Your feedback, ideas and bug reports are very welcome. If you would like to get in touch with us, consider joining our `openATTIC Users <https://groups.google.com/forum/#%21forum/openattic-users>`_ Google Group, visit our `#openattic <irc://irc.freenode.org/#openattic>`_ channel on `irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_ or leave comments below this blog post.

See the list below for a more detailed change log and further references. The OP codes in brackets refer to individual Jira issues that provide additional details on each item. You can review these on our `public Jira instance <http://tracker.openattic.org/>`_.

Changelog for version 3.4.2
----------------------------

Added
~~~~~
* Backend: Enable compression on Ceph pools on BlueStore OSD Pools (:issue:`OP-2309`)
* Backend: The data_pool parameter will now be recognized by creating an RBD (:issue:`OP-2317`)
* Backend: Added settings to allow overwriting the cluster by DeepSea (:issue:`OP-2502`, :issue:`OP-2483`, :issue:`OP-2339`)
* WebUI: iSCSI - Add help text on Images field (:issue:`OP-2508`)
* WebUI: Add support for local configs (:issue:`OP-2469`)
* WebUI: Make Ceph keyring configurable from the UI (:issue:`OP-2504`)
* WebUI: RBD - Add information about features unsupported by iSCSI (:issue:`OP-2522`)
* Installation: Add `SALT_API_*` and `RGW_API_*` to the openATTIC sysconfig file (:issue:`OP-2280`)
* WebUI: Add helper component (:issue:`OP-2527`)
* WebUI: Create ec overwrite enabled pools in the UI (:issue:`OP-2357`)
* WebUI: Calculate the best pg size for a pool (:issue:`OP-2377`)

Changed
~~~~~~~
* WebUI: Migration of Ceph RBDs controllers to components (:issue:`OP-2400`)
* Backend/QA: RGW proxy Gatling tests are disabled by default (:issue:`OP-2525`)
* WebUI: Improve suggested iSCSI target IQN format (:issue:`OP-2511`)
* Development: Remove Vagrant required-changes.patch file (:issue:`OP-1446`)
* Backend: Cached Grafana CSS to optimized the iframe loading performance (:issue:`OP-2509`)
* WebUI: Enabled crush rule set for all pool types (:issue:`OP-2378`)

Fixed
~~~~~
* Backend: DeepSea's pillar data may not contain `roles` for a minion (:issue:`OP-2507`)
* Backend: Allow `CephRbd.features` to be empty (:issue:`OP-2506`)
* WebUI: Allow the deletion of non-empty S3 buckets (:issue:`OP-2503`)
* WebUI: The copy to clipboard button does not work with password fields (:issue:`OP-2501`)
* WebUI: Fix various issues when no Ceph cluster can be loaded. Additionally add a visual feedback while loading the Ceph clusters (:issue:`OP-2493`)
* Backend: Fixed ceph django unit tests (:issue:`OP-2526`)
* Backend: Use the correct username and keyring to connect to CephFS (:issue:`OP-2515`)
* WebUI: Remove the use of the old format in RBD form (:issue:`OP-2538`)
