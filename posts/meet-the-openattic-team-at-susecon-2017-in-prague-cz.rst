.. title: Meet the openATTIC team at SUSECON 2017 in Prague (CZ)
.. slug: meet-the-openattic-team-at-susecon-2017-in-prague-cz
.. date: 2017-09-19 20:33:09 UTC+02:00
.. tags:  ceph, community, conference, development, openattic, storage, suse, susecon
.. category: 
.. link: 
.. description:  Speaking about openATTIC 3.x at SUSECON in Prague, CZ on 2017-09-26
.. type: text
.. author: Lenz Grimmer

.. image:: /images/susecon-2017-logo.png

Conference season is heating up again - next week `Kai Wagner
<https://github.com/capri1989>`_ (co-founder of the openATTIC project) and
myself will be at `SUSECON in Prague <http://www.susecon.com/>`_, to `talk about
<https://susecon17.smarteventscloud.com/connect/sessionDetail.ww?SESSION_ID=119040>`_
how to manage a Ceph cluster with SUSE's Salt-based `DeepSea
<https://github.com/SUSE/DeepSea>`_ project and openATTIC 3.x.

SUSECON marks an important milestone in our project's history - it was at last
year's SUSECON in Washington, DC at which the :doc:`acquisition of the openATTIC
project and team <openattic-joins-suse>` by SUSE was announced.

A lot has changed in the meanwhile - among other things, the team has grown
significantly (`Ricardo Marques <https://github.com/ricardoasmarques>`_ and
`Tiago Melo <https://github.com/tspmelo>`_ from Portugal and `Volker Theile
<https://github.com/votdev>`_ from Germany joined us), we :doc:`switched from
Mercurial to git <openattic-code-repository-migrated-to-git>` and changed the
project's focus in version 3.x into becoming a :doc:`scalable, feature-rich Ceph
management tool
<implementing-a-more-scalable-storage-management-framework-in-openattic-30>`.

We look forward to show off what we have worked on and achieved in the
meanwhile. If you haven't done so already - make sure to `register for SUSECON
<https://susecon17.smarteventscloud.com/portal/login.ww>`_ right away! See you
next week!