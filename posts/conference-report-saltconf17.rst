.. title: Conference Report: SALTCONF17
.. slug: conference-report-saltconf17
.. date: 2017-11-08 08:51:14 UTC-07:00
.. tags: conference, event, saltstack, deepsea, openattic
.. category:
.. link:
.. description: A summary of last week SALTCONF17, in Salt Lake City
.. type: text
.. author: Ricardo Marques

.. image:: /galleries/SALTCONF17/saltconfbonw.png

Last week SALTCONF17 took place at Salt Lake City and I was one of the
attendees of this great event which included 2 days of pre-conference
training and 2 days of conference.

Salt Palace Convention Center was the chosen place for this conference that
included 60 keynotes and breakout sessions with dozens of SaltStack use cases
presented by SaltStack users and in-depth tech talks from the SaltStack
developers and experts.

The SaltStack Certified Engineer exam was available to attendees that took the
pre-conference training (learn more about the `SSCE certification and exam here
<https://saltstack.com/certification>`_).

.. TEASER_END

During those days I had the opportunity to learn more about SaltStack best
practices, architecture, and also about SaltStack Enterprise which includes a
great web GUI that simplifies the management and usage of SaltStack.

In `openATTIC <https://www.openattic.org>`_ we consume a lot of
services from `DeepSea <https://github.com/SUSE/DeepSea>`_, using the Salt
REST API.

This means that we work very closely and also contribute to this SaltStack
based solution to deploy and manage `ceph <http://ceph.com>`_ clusters, so
it was really nice to have the opportunity to spend some time with the
SaltStack community.

Sponsored by companies like SUSE, Adobe and DigiCert, the SaltConf17 conference
also included a party with some VR gaming, food, drinks, and awesome swag from
SaltStack and sponsors.

I took some pictures that I would like to share:

.. slides::
  /galleries/SALTCONF17/IMG_2230.HEIC.jpg
  /galleries/SALTCONF17/IMG_2199.HEIC.jpg
  /galleries/SALTCONF17/IMG_2202.HEIC.jpg
  /galleries/SALTCONF17/IMG_2258.HEIC.jpg
  /galleries/SALTCONF17/IMG_2339.HEIC.jpg
  /galleries/SALTCONF17/IMG_2226.HEIC.jpg

At the end of the conference we also had a great open Q&A with Thomas Hatch,
the creator of SaltStack.

In summary, SaltConf17 was a very good conference to attend and Salt Lake City
is a pretty nice place.

